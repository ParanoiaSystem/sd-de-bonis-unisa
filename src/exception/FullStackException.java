package exception;

@SuppressWarnings("serial")
public class FullStackException extends Exception{
	public FullStackException(String string){
		super(string);
	}
}
