package exception;

@SuppressWarnings("serial")
public class EmptyStackException extends Exception {
	public EmptyStackException(String string) {
		super(string);
	}
}
